\documentclass[10pt, a4paper, reqno]{amsart}
\usepackage[ngerman]{babel}
\usepackage{enumitem}
\usepackage{xcolor}

\usepackage{caption}
\captionsetup[figure]{name=Abbildung}
\usepackage{graphicx}
\graphicspath{ {./images/} }
\usepackage{float}

\usepackage{physics}
\usepackage{tipa}
\newcommand{\overarc}[1]{{%
    \setbox9=\hbox{#1}%
    \ooalign{\resizebox{\wd9}{\height}{\texttoptiebar{\phantom{A}}}\cr#1}}}

\usepackage{amsmath, amsfonts, amssymb}
\usepackage{amsthm, thmtools}
\declaretheorem[name=Aufgabe, thmbox = L]{aufgabe}
\declaretheorem[name=Lemma]{lemma*}
\counterwithin*{lemma*}{aufgabe}
\counterwithin*{equation}{aufgabe}
\newcommand{\aufgabelabelname}{\theaufgabe . Aufgabe}
\renewcommand\qedsymbol{$\blacksquare$}
\renewcommand\proofname{Beweis}

\makeatletter
\renewenvironment{proof}[1][\proofname]{\par
  \pushQED{\qed}%
  \normalfont \topsep6\p@\@plus6\p@\relax
  \trivlist
  \item\relax
  {\bfseries#1}\hspace\labelsep\ignorespaces
}{%
  \popQED\endtrivlist\@endpefalse
}

% TODO: Nur „(Siehe S.$X)“ hinzufügen, wenn der Beweis auf einer anderen Seite liegt
\newenvironment{lemma}[1]{
  \begin{lemma*}
    \textnormal{(\textbf{Siehe S.#1})}
    % \ifnum\thepage=\value{#1}
    % \else
    % \textnormal{(\textbf{Siehe S.#1})}
    % \fi
  }{
  \end{lemma*}
}

\newenvironment{proof_thm}[1]{
  \begin{proof}[\proofname~(#1)]
  }{
  \end{proof}
}

\newenvironment{solution}{
  \par%
  \normalfont \topsep6\p@\@plus6\p@\relax
  \trivlist
  \item\relax
  {\bfseries Lösung}\hspace\labelsep\ignorespaces
}

\makeatother

\usepackage{lastpage}
\usepackage{fancyhdr}
\fancyheadoffset{0cm}
\fancyhf{}
\fancypagestyle{hilfsmittel} {
  \fancyhead[R]{\fontsize{9}{7}Valentin Herrmann}
  \fancyfoot[C]{\thepage /\pageref{LastPage}}
}
\fancypagestyle{normal} {
  \fancyhead[C]{\fontsize{9}{7}\large \aufgabelabelname}
  \fancyhead[R]{\fontsize{9}{7}Valentin Herrmann}
  \fancyfoot[C]{\thepage /\pageref{LastPage}}
}

\usepackage[
left=6cm,
right=2cm,
footskip=3em,
twoside=false,
xetex
]{geometry}
\usepackage{unicode-math}

% hyperref must be the last package in the preamble
\usepackage{hyperref}
\hypersetup{
  bookmarks=true,
  unicode=true,
  pdfborder={0 0 0},
  pdfstartview={FitH},
  pagebackref=true,
  colorlinks=true,
  urlcolor=blue,
  linkcolor=black,
  linktoc=all
}
\usepackage{xurl}

\renewcommand{\figureautorefname}{Abbildung}

\newcommand{\ovli}{\overline}
\newcommand\psin[1]{\text{sin}(#1)}

% Number only certain lines in an align environment
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

\newcommand{\fig}[2]{
  \begin{figure}[h]
    \centering
    \includegraphics[width=#1\textwidth]{#2}
  \end{figure}
}

\begin{document}
\thispagestyle{hilfsmittel}
\vspace*{3pt}
\begin{center}
  \large{\textbf{Hilfsmittel:}}
\end{center}
\vspace*{3pt}

\paragraph{Aufgabe 4.}
\begin{enumerate}
  \item Beweis für die Eindeutigkeit des kleinsten umschließenden Kreises:\\
        \url{https://en.wikipedia.org/wiki/Smallest-circle_problem#Characterization},
        zuletzt aufgerufen am 31. August 2022
\end{enumerate}

\newpage
\pagestyle{normal}
\begin{aufgabe}
  Bestimme alle Quadrupel \((a,b,c,d)\) positiver reeller Zahlen, die die beiden folgenden Gleichungen erfüllen:
  \begin{align}
    ab+cd &= 8,\label{eq:1}\\
    abcd &= 8+a+b+c+d\label{eq:2}
  \end{align}
\end{aufgabe}

\begin{solution}
  Nur das Quadrupel \((2,2,2,2)\) erfüllt die Gleichungen.
\end{solution}

\begin{proof}
  Im folgenden Beweis wird mehrmals die AM-GM Ungleichung auf Terme aus \(a,b,c,d\) angewendet. Dies ist möglich, da \(a,b,c,d\) alle positive Zahlen sind.

  Zuerst beweisen wir, dass \((2,2,2,2)\) wirklich eine Lösung ist. Setzen wir ein: \eqref{eq:1}: \(ab+cd = 2·2+2·2 = 8\); \eqref{eq:2}: \(abcd = 2^4=16=8+2·4 = 8 + a + b + c + d\). \((2,2,2,2)\) ist also eine Lösung.

  Beweisen wir nun, dass keine andere Lösung für das Gleichungssystem existiert. Folgern wir aus~\eqref{eq:1} mit der AM-GM Ungleichung:
  \begin{align*}
    8 &= ab + cd ≥ 2\sqrt{abcd} \\
    ⇒ 4 &≥ \sqrt{abcd}
  \end{align*}
  Da beide Seiten positiv sind, können wir sie quadrieren ohne das Vergleichszeichen zu ändern:
  \begin{equation}
    \label{eq:3}
    16 ≥ abcd
  \end{equation}
  Schließen wir nun aus~\eqref{eq:2} wieder mit der AM-GM Ungleichung:
  \begin{align*}
    abcd &= 8+a+b+c+d\\
         &= 2+2+2+2+a+b+c+d \numberthis\label{eq:7}\\
         &≥ 8\sqrt[8]{2^4abcd}\\
         &= 2^{\frac{7}{2}}\sqrt[8]{abcd}
  \end{align*}
  Nun teilen wir die Gleichung durch \(\sqrt[8]{abcd}\):
  \begin{flalign*}
    &&  (abcd)^{\frac{7}{8}} &≥ 2^{\frac{7}{2}}&& \text{| \(x^{\frac{8}{7}}≥y^{\frac{8}{7}}\), wenn \(x≥y≥0\) }\\
    &&⇒ abcd &≥ 16 \numberthis\label{eq:5}
  \end{flalign*}
  Aus~\eqref{eq:3} und~\eqref{eq:5} folgt \(abcd = 16\). Bei der AM-GM Ungleichung gilt bekanntlich Gleichheit, wenn alle Operanden gleich groß sind. Aus~\eqref{eq:7} schließen wir also \( a=b=c=d=2 \). \((2,2,2,2)\) ist also wirklich die einzige Lösung.
\end{proof}

\newpage

\begin{aufgabe}
  Auf dem Tisch liegen 2022 Streichhölzer und ein handelsüblicher Spielwürfel, bei dem die Augenzahl \(a\) oben liegt. Nun spielen Max und Moritz folgendes Spiel:

  Sie nehmen abwechselnd nach folgender Regel Streichhölzer weg, wobei Max beginnt: Wer am Zug ist, kippt den Würfel über eine Kante seiner Wahl und nimmt dann genau so viele Streichhölzer weg, wie die nun oben liegende Seite des Würfels anzeigt. Wer keinen regelgerechten Zug mehr ausführen kann, hat verloren.

  Für welche \(a\) kann Moritz erzwingen, dass Max verliert?

  \vspace{0.5ex}
  \noindent \footnotesize{Anmerkung: Die Richtigkeit der Antwort ist zu begründen.}\\
  \noindent \footnotesize{Hinweis: Auf den Seiten eines handelsüblichen Würfels stehen die Augenzahlen 1 bis 6, wobei die Summe der Augenzahlen auf gegenüber liegenden Seiten stets 7 beträgt.}
\end{aufgabe}

\begin{solution}
  Max kann für alle \(a\) den Sieg erzwingen, Moritz kann es also nie.
\end{solution}

\begin{proof}
  Im folgenden Beweis steht das Folgenglied \(z_n\) für die Menge der oben liegenden Augenzahlen des Würfels, bei welchen der ziehende Spieler den Sieg nicht erzwingen kann, solange momentan \(n\) Streichhölzer auf dem Tisch liegen. Des Weiteren werden auf dem Würfel gegenüberliegende Zahlen als komplementär bezeichnet.

  Wir werden nun mit vollständiger Induktion beweisen, dass für \(n∈ℕ_{≥2}\)
  \begin{equation}
    \label{eq:4}
    z_n =
    \begin{cases}
      \{\} &\text{für } n \bmod 9 = 1,2,6,7 \\
      \{2,5\}     &\text{für } n \bmod 9 = 5\\
      \{3,4\}     &\text{für } n \bmod 9 = 3,4,8\\
      \{1,2,3,4,5,6\}      &\text{für } n \bmod 9 = 0
    \end{cases}
  \end{equation}
  gilt.

  Im Beweis werde ich eine kleine Sprache verwenden um Abschnitte der Folge \(z\) knapp darzustellen. Die Sprache basiert auf einer Aneinanderreihung von Symbolen, welche benachbarten Folgenelementen entsprechen. Dabei werden die Folgenelemente mit der Funktion \(k\) folgendermaßen konvertiert:
  \begin{equation*}
    k :
    \begin{cases}
      \{\}            &→ \texttt{„·“}\\
      \{1,6\}         &→ \texttt{„1“}\\
      \{2,5\}         &→ \texttt{„2“}\\
      \{3,4\}         &→ \texttt{„3“}\\
      \{1,2,3,4,5,6\} &→ \texttt{„—“}
    \end{cases}
  \end{equation*}
  \(z\) sollte nach \eqref{eq:4} für \(n≥2\) also einer Wiederholung der Teilfolge \texttt{„·332··3—·“} entsprechen.

  \begin{itemize}[label=$\lozenge$, itemsep=2ex]
    \item \emph{Induktionsanfang}: \\
          Stellen wir zunächst fest, dass der ZS, der ziehende Spieler, stets verliert, wenn nur noch keine Streichhölzer mehr auf dem Tisch liegen. Damit gilt also \(z_0=\{1,2,3,4,5,6\}\). Des Weiteren gilt \(z_1 = \{1,6\}\), denn wenn nur noch ein Streichholz liegt, muss der ZS, der ziehende Spieler, zu 1 kippen um zu gewinnen. Offensichtlich geht das genau dann nicht, wenn 1 oder 6 auf der Oberseite des Würfels liegt.

          Nun werden wir zeigen, dass~\eqref{eq:4} für \(2≤n≤7\) gilt. Wir werden für bessere Verständlichkeit die Teilfolge \(z_0…z_{n-1}\) in der vorher definierten Sprache darstellen. Die Elemente, zu denen der ZS kippen kann, sodass er den Sieg weiterhin erzwingen kann, werden unterstrichen.

          \begin{itemize}
            \item \(n=2\): Vorherige Elemente: \texttt{„\underline{—1}“}:
                  Der ZS kann nur zu 1 oder 2 kippen ohne direkt zu verlieren. Kippt er zu 2 (\texttt{„—“}), so gewinnt er sofort. Kippt er zu 1 (\texttt{„1“}), so kann der nächste Spieler nur verlieren, da \(2-1 ∈ z_1\). Da 1 \& 2 keine komplementären Zahlen am Würfel sind, kann der ZS stets zu einer der beiden kippen. Daher gilt \(k(z_2) = k(\{\}) = \texttt{„·“}\).
            \item \(n=3\): VE: \texttt{„\underline{—}1·“}:
                  Der ZS kann nur zu 1, 2 oder 3 kippen. Anhand der VE ist erkennbar, dass der ZS \textbf{nur} den Sieg erzwingen kann, wenn er zu \(3\) (\texttt{„—“}) kippt. Damit gilt \(k(z_3) = k(\{3,4\}) = \texttt{„3“}\), schließlich kann er auch mit einer \(4\) oben auf dem Würfel nicht zur \(3\) kippen.
            \item \(n=4\): VE: \texttt{„\underline{—}1·3“}:
                  Der ZS kann zu 1, 2, 3 oder 4 kippen. Der ZS kann den Sieg \textbf{nur} erzwingen, wenn er zu \(4\) (\texttt{„—“}) kippt. Damit gilt \(k(z_4) = k(\{3,4\}) = \texttt{„3“}\).
            \item \(n=5\): VE: \texttt{„\underline{—}1·33“}:
                  Der ZS kann zu 1, 2, 3, 4 oder 5 kippen. Der ZS kann den Sieg \textbf{nur} erzwingen, wenn er zu \(5\) (\texttt{„—“}) kippt. Damit gilt \(k(z_5) = k(\{2,5\}) = \texttt{„2“}\).
            \item \(n=6\): VE: \texttt{„\underline{—}1·\underline{3}32“}:
                  Der ZS kann den Sieg erzwingen, wenn er zu \(3\) (\texttt{„3“}) oder \(6\) (\texttt{„—“}) kippt. 3 \& 6 sind keine komplementären Zahlen am Würfel. Damit gilt \(k(z_6) = k(\{\}) = \texttt{„·“}\).
            \item \(n=7\): VE: \texttt{„—\underline{1}·\underline{3}32·“}:
                  Der ZS kann den Sieg erzwingen, wenn er zu \(4\) (\texttt{„3“}) oder \(6\) (\texttt{„1“}) kippt. 4 \& 6 sind keine komplementären Zahlen am Würfel. Damit gilt \(k(z_7) = k(\{\}) = \texttt{„·“}\).
          \end{itemize}
    \item \emph{Induktionsschritt}: \\
          \eqref{eq:4} sei nun für alle \(z_k\) mit \(n-6≤k≤n-1, k ∈ ℕ\) erfüllt. Beweisen wir nun mit einer Unterscheidung der Fälle von~\eqref{eq:4}, dass~\eqref{eq:4} für \(n∈ℕ_{≥8}\) hält. Wir werden dabei in der vorher definierten Sprache den Abschnitt \(z_{n-6}…z_{n-1}\) darstellen. Die Wahlen, welche sicher stellen, dass der ZS den Sieg erzwingen kann, werden wieder unterstrichen:
          \begin{itemize}
            \item \(n ≡ 0 \pmod{9}\): VE: \texttt{„332··3“}:
                  Anhand der VE ist erkennbar, dass der ZS den Sieg in dieser Situation nicht erzwingen kann. Damit gilt \(k(z_n) = k(\{1,2,3,4,5,6\}) = \texttt{„—“}\).
            \item \(n ≡ 1 \pmod{9}\): VE: \texttt{„3\underline{2}··3\underline{—}“}:
                  Der ZS kann den Sieg erzwingen, wenn er zu \(1\) (\texttt{„—“}) oder \(5\) (\texttt{„2“}) kippt. 1 \& 5 sind keine komplementären Zahlen am Würfel. Damit gilt \(k(z_n) = k(\{\}) = \texttt{„·“}\).
            \item \(n ≡ 2 \pmod{9}\): VE: \texttt{„2··\underline{3}\underline{—}·“}:
                  Der ZS kann den Sieg erzwingen, wenn er zu \(2\) (\texttt{„—“}) oder \(3\) (\texttt{„3“}) kippt. 2 \& 3 sind keine komplementären Zahlen am Würfel. Damit gilt \(k(z_n) = k(\{\}) = \texttt{„·“}\).
            \item \(n ≡ 3 \pmod{9}\): VE: \texttt{„··\underline{3}\underline{—}··“}:
                  Der ZS kann den Sieg \textbf{nur} erzwingen, wenn er zu \(3\) (\texttt{„—“}) oder \(4\) (\texttt{„3“}) kippt. 3 \& 4 sind komplementäre Zahlen am Würfel. Damit gilt \(k(z_n) = k(\{3,4\}) = \texttt{„3“}\).
            \item \(n ≡ 4 \pmod{9}\): VE: \texttt{„·3\underline{—}··3“}:
                  Der ZS kann den Sieg \textbf{nur} erzwingen, wenn er zu \(4\) (\texttt{„—“}) kippt. Damit gilt \(k(z_n) = k(\{3,4\}) = \texttt{„3“}\).
            \item \(n ≡ 5 \pmod{9}\): VE: \texttt{„3\underline{—}··33“}:
                  Der ZS kann den Sieg \textbf{nur} erzwingen, wenn er zu \(5\) (\texttt{„—“}) kippt. Damit gilt \(k(z_n) = k(\{2,5\}) = \texttt{„2“}\).
            \item \(n ≡ 6 \pmod{9}\): VE: \texttt{„\underline{—}··\underline{3}32“}:
                  Der ZS kann den Sieg erzwingen, wenn er zu \(3\) (\texttt{„3“}) oder \(6\) (\texttt{„—“}) kippt. 3 \& 6 sind keine komplementären Zahlen am Würfel. Damit gilt \(k(z_n) = k(\{\}) = \texttt{„·“}\).
            \item \(n ≡ 7 \pmod{9}\): VE: \texttt{„··\underline{3}\underline{3}\underline{2}·“}:
                  Der ZS kann den Sieg erzwingen, wenn er zu \(2\) (\texttt{„2“}) oder \(3\) (\texttt{„3“}) (oder \(4\)) kippt. 2 \& 3 sind keine komplementären Zahlen am Würfel. Damit gilt \(k(z_n) = k(\{\}) = \texttt{„·“}\).
            \item \(n ≡ 8 \pmod{9}\): VE: \texttt{„·3\underline{3}2··“}:
                  Der ZS kann den Sieg \textbf{nur} erzwingen, wenn er zu \(4\) (\texttt{„3“}) kippt. Damit gilt \(k(z_n) = k(\{3,4\}) = \texttt{„3“}\).
          \end{itemize}
  \end{itemize}
  Damit haben wir also~\eqref{eq:4} bewiesen. Direkt folgt \(z_{2022} = z_{6+9k} = \{\}\) für alle \(k∈ℕ\). Max beginnt, er kann also bei allen oben liegenden Augenzahlen einen Sieg erzwingen, damit kann Moritz es nie.
\end{proof}

\newpage

\begin{aufgabe}
  Im spitzwinkligen Dreieck \(ABC\) mit \(\overline{AC}<\overline{BC}\) seien \(m_a\) und \(m_b\) die Mittelsenkrechten auf den Seiten \(BC\) bzw. \(AC\), ferner \(M_C\) der Mittelpunkt der Seite \(AB\). Die Seitenhalbierende \(CM_c\) schneide \(m_a\) im Punkt \(S_a\) und \(m_b\) im Punkt \(S_b\); die Geraden \(AS_b\) und \(BS_a\) schneiden sich im Punkt \(K\).

  Beweise: \(\angle ACM_c=\angle KCB\)
\end{aufgabe}

\begin{proof}
  \begin{center}
    \includegraphics[width=0.7\textwidth]{a3-3}
  \end{center}
  Definieren wir zunächst \(O\) als den Umkreis vom Dreieck \(ABC\). Spiegeln wir nun \(ΔABC\) an der Mittelsenkrechten \(m_a\). Das neue Dreieck nennen wir vorerst \(A'C'B'\). Offensichtlich wird \(B\) auf \(C = B'\), \(C\) auf \(B = C'\) gespiegelt, d.h. \(BC = C'B'\). Daher werden wir das neue Dreieck als \(A'BC\) notieren. Zudem sind die Umkreise von \(ΔABC\) und \(ΔA'BC\) äquivalent, denn der Umkreismittelpunkt von \(ΔABC\) liegt bekanntlich auf \(m_a\), d.h. er wird auf sich selbst gespiegelt. Die Spiegelung von \(M_c\) wird \(M_{c'}\) genannt. Da nach Definition \(S_a\) auf \(m_a\) liegt, ist \(S_a\) äquivalent zu seiner Spiegelung. Also liegt \(M_{c'}\) auf \(S_aB\), denn \(M_{c'}\)s Urbild \(M_c\) liegt auf der Gerade \(S_a'B' = S_aC\). Äquivalent ist \(ΔAB''C\) die Spiegelung von \(ΔABC\), sowie \(M_{c''}\) die Spiegelung von \(M_c\)  an \(m_b\). Dabei liegt \(M_{c''}\) auf \(S_b'C'' = S_bA\). Anders als in der Skizze kann \(K\) durchaus außerhalb von \(ΔA'BC\) liegen.

  Stellen wir noch fest, dass \(M_{c'} ≠ M_{c''}\) gilt, denn wären die Punkte äquivalent, so wäre \(M_{c}\) von verschiedenen Mittelsenkrechten auf den gleichen Punkt gespiegelt worden. Das ist allerdings nur möglich, wenn \(M_c\) auf den Mittelsenkrechten \(m_a\) und \(m_b\) liegt. In diesem Fall ist \(M_c\) allerdings der Umkreismittelpunkt von \(ΔABC\). Doch da \(ΔABC\) spitzwinklig ist, kann der Umkreismittelpunkt nicht auf dem Dreieck, sondern nur in ihm liegen. Also können die beiden Punkte nicht äquivalent sein.

  Halten wir zunächst fest, dass \(ΔBM_{c'}A' ≅ ΔAM_cC ≅ ΔAM_{c''}C\) gilt, schließlich sind Dreiecke kongruent zu ihren Spiegelbildern. Stellen wir nun fest, dass wenn \(K\) auf einem der Punkte \(M_{c''},M_{c'},C\) liegt, die vier Punkte offensichtlich auf einem Kreis liegen. Ist dem nicht der Fall so sind alle 4 Punkte unterschiedlich und wir folgern mit Hilfe von Winkeln modulo 180° und der Kongruenz von \(ΔBM_{c'}A'\) und \(ΔAM_{c''}C\): Die Winkel \(\angle A'M_{c'}B ≡ 180° - \angle BM_{c'}C ≡ \angle CM_{c'}B ≡ \angle CM_{c'}K \) und \(\angle CM_{c''}A ≡ \angle CM_{c''}K \) sind gleich groß (modulo 180°). Aus \(\angle CM_{c''}K ≡ \angle CM_{c'}K \pmod{180°}\)  folgt bekanntlich sofort, dass \(M_{c''}, C, M_{c'}, K\) (allerdings nicht notwendigerweise in dieser Reihenfolge) zusammen auf einem Kreis liegen.

  \begin{center}
    \includegraphics[width=0.7\textwidth]{a3-2-2}
  \end{center}
  Führen wir nun \(O\), den Umkreis von \(ΔABC\), mit Hilfe einer zentrischen Streckung mit Faktor \(0.5\) und Zentrum \(C\) in einen neuen Kreis \(o\) um. Offensichtlich liegen \(M_a\), \(M_b\), \(M_{c'}\), \(M_{c''}\) als Bilder von \(B\), \(A\), \(A'\), \(B''\) auf \(o\). Auch \(C\) liegt auf dem Kreis, da der Punkt zu sich selbst abgebildet wird. Da jeder Kreis bekanntlich durch 3 verschiedene auf dem Kreis liegende Punkte eindeutig bestimmt ist, ist \(o\) der gleiche Kreis auf dem auch \(M_{c''},C,M_{c'}, K\) liegen.

  Stellen wir noch fest, dass \(K\) nicht gleich \(C\) ist, denn dann müsste \(K\) auf \(BC\) liegen, womit auch \(M_{c'}\) und daher \(A\) auf \(BC\) liegen müsste. Dann wäre \(ABC\) allerdings kein Dreieck. Zudem ist \(M_a\) nicht gleich \(M_{c'}\), denn in diesem Fall wäre \(A'\) gleich \(B\), daher \(A\) gleich \(C\), und damit \(ABC\) kein Dreieck. Zuletzt ist noch \(M_{c'}\) ungleich \(B\). Wären die Punkte gleich, so läge \(A\) auf \(BC\) und damit wäre \(ABC\) kein Dreieck.

  Zunächst gilt nach dem Umfangswinkelsatz \(\angle KCM_a = \angle KM_{c'}M_a = \angle BM_{c'}M_a\) solange \(K\) auf \(BM_{c'}[\) liegt. Liegt \(K\) weder auf dem vorherigen Abschnitt noch auf \(M_{c'}\), so gilt nach der Winkelsumme der gegenüberliegenden Winkel im Sehnenviereck \(\angle KCM_a = 180° - \angle M_aM_{c'}K = \angle BM_{c'}M_a \). Liegt \(K\) auf \(M_{c'}\) so gilt nach dem Sehnentangentenwinkelsatz \(\angle KCM_a = \angle BM_{c'}M_a \). Dieser Satz ist anwendbar, da nach der Umkehrung des Sehnentangentenwinkelsatzes \(BM_{c'}\) eine Tangente von \(o\) ist, schließlich gilt \(\angle CM_{c''}M_{c'} ≡ \angle CM_{c''}K ≡ \angle CM_{c''}A ≡ \angle A'M_{c'}B \pmod{180°}\) (Kongruenz von \(ΔBM_{c'}A'\) \& \(ΔAM_{c''}C\), gerichtete Winkel modulo 180° um die Reihenfolge von \(A,K,M_{c'}\) nicht beachten zu müssen). Des Weiteren ist \(M_aM_{c'}\) die Bildgerade von \(BA'\) unter vorheriger Streckung. \(M_aM_{c'}\) und \(BA'\) sind also bekanntlich parallel. Damit sind \(\angle BM_{c'}M_a\) und \(\angle M_{c'}BA'\) Wechselwinkel. Zuletzt ist \(ΔA'BM_{c'}\) die Spiegelung von \(ΔM_cCA\), d.h. \(\angle M_{c'}BA' = \angle ACM_c \). Setzen wir zusammen:
  \begin{equation*}
    \angle KCB = \angle KCM_a = \angle BM_{c'}M_a = \angle M_{c'}BA' = \angle ACM_c
  \end{equation*}
\end{proof}

\newpage

\begin{aufgabe}
  In der Ebene seien einige Punkte rot und einige blau gefärbt. Der Abstand zwischen zwei verschieden gefärbten Punkten ist immer höchstens 1.

  Beweise: Es gibt einen Kreis mit Durchmesser \(\sqrt{2}\) derart, dass außerhalb dieses Kreises keine zwei verschieden gefärbte Punkte liegen.

  Anmerkung: Es genügt, dies für den Fall zu zeigen, dass endlich viele Punkte gefärbt wurden.
\end{aufgabe}

Im folgenden Beweis ist mit Kreisscheibe eine abgeschlossene Kreisscheibe gemeint.

\begin{lemma}{\pageref{proof_thm:4:1}}
  \label{lemma:4:1}
  Der kleinste umschließende Kreis einer endlichen, nicht leeren Menge an Punkten in der Ebene ist stets eindeutig.
\end{lemma}

\begin{lemma}{\pageref{proof_thm:4:5}}
  \label{lemma:4:5}
  Eine Schnittfläche von verschiedenen Kreisscheiben mit Radius \(r_1\) ist Teilmenge einer Kreisscheibe \(k\) mit Radius \(r_2≤r_1\), solange die Ecken der Schnittfläche innerhalb von \(k\) liegen.
\end{lemma}

\begin{lemma}{\pageref{proof_thm:4:6}}
  \label{lemma:4:6}
  Alle folgenden Kreisscheiben haben den gleichen Radius. Die Schnittmenge zweier Kreisscheiben ist Teilmenge jeder Kreisscheibe die zwischen den Kreisscheiben liegt, deren Mittelpunkt also auf der Verbindungsstrecke der Mittelpunkte der zwei Kreisscheiben liegt.
\end{lemma}

\begin{lemma}{\pageref{proof_thm:4:7}}
  \label{lemma:4:7}
  Alle folgenden Kreisscheiben haben den gleichen Radius. Die Schnittmenge von drei Kreisscheiben ist Teilmenge der Punktemenge jeder Kreisscheibe, deren Mittelpunkt innerhalb des Dreiecks der Mittelpunkte der drei Kreisscheiben liegt.
\end{lemma}

\begin{lemma}{\pageref{proof_thm:4:9}}
  \label{lemma:4:9}
  Liegen mindestens 3 Punkte einer Menge \(P\) auf dem entsprechenden kleinsten umschließenden Kreis von \(P\), so bildet mindestens ein Tripel an Punkten auf dem Kreis ein spitzes Dreieck.
\end{lemma}

\begin{proof}
  Offensichtlich existiert der gesuchte Kreis genau dann, wenn mindestens eine der Mengen gleichgefärbter Punkte in einen Kreis mit Durchmesser \(\sqrt{2}\) passt.

  Definieren wir nun \(M_r\) als die Menge der roten Punkte und \(M_b\) als die der blauen. Nach \autoref{lemma:4:1} ist der kleinste umschließende Kreis \(K\) von \(M_r\) eindeutig, denn weder \(M_r\) noch \(M_b\) sind leer, da in der Ebene \textbf{einige} Punkte pro Farbe angemalt wurden.

  Unterscheiden wir nun den kleinsten umschließenden Kreis anhand der Anzahl an Punkten, welche auf ihm liegen.

  Liegt nur ein Punkt auf \(K\), so ist \(K\) offensichtlich entartet, da \(K\)s Radius gleich Null sein muss. In diesem Fall besteht \(M_r\) also nur aus diesem einen Punkt und passt offensichtlich in einen Kreis mit Durchmesser \(\sqrt{2}\).

  Liegen nur zwei Punkte auf \(K\), so muss deren Verbindungsstrecke offensichtlich einen Durchmesser des Kreises bilden.

  Liegen mehr Punkte auf \(K\), so wählen wir von diesen schlicht 3 Punkte, welche kein stumpfwinkliges Dreieck bilden. Mindestens ein solches Dreieck existiert nach \autoref{lemma:4:9}.

  In diesen beiden Fällen, also wenn \(K\) durch 2 oder 3 Punkte definierbar ist, betrachten wir zunächst den Radius \(r_K\) von \(K\). Gilt \(r_K≤\sqrt{2}\), so passt \(M_r\) offensichtlich in einen Kreis der gesuchten Größe. Ist der Radius zu groß, so müssen wir beweisen, dass \(M_b\) in einen Kreis der gesuchten Größe passt.

  Definieren wir zunächst \(P_K\) als Menge der 2 bis 3 Punkte, welche \(K\) definieren. Strecken wir nun \(K\), \(P_K\) zu \(k\), \(P_k\) mit dem Streckungsfaktor \(\frac{\sqrt{2}}{r_K}\) und dem Zentrum \(O_K\), also dem Mittelpunkt des Kreises \(K\).

  Des Weiteren liegt die Figur \(P_k\)s, also Linie oder Dreieck, innerhalb der Figur \(P_K\)s. Denn die Streckung hat die Figur verkleinert (\(\frac{\sqrt{2}}{r_K} < 1\)) und in beiden Fällen ist die Figur konvex, zudem liegt das Zentrum der Streckung \(O_K\) innerhalb/auf der Figur. Im Falle von 2 Punkten liegt \(O_K\) auf der Figur, da die Verbindungsstrecke der 2 Punkte ein Durchmesser des Kreises ist (wie vorher festgestellt), bei 3 Punkten liegt \(O_{K}\) in/auf der Figur, da das Dreieck nicht stumpfwinklig ist (wie vorher festgestellt), weshalb bekanntlich der Mittelpunkt des Umkreises, welcher bei nicht stumpfwinkligen Dreiecken dem kleinsten umschließenden Dreiecks entspricht, in/auf dem Dreieck liegt.

  Definieren wir nun \(p\) als Funktion, welche einer Punktemenge eine zweite Menge zuordnet, dessen Punkte von allen Punkten der gegebenen Menge maximal 1 entfernt sind. \(p\) bezeichnet also die Schnittfläche der Kreisscheiben mit Radius 1 welche an den Punkten der gegebenen Menge zentriert sind. Um den Beweis zu schließen, müssen wir also beweisen, dass \(p(P_K)\) eine Teilmenge einer Kreisscheibe mit Radius \(\sqrt{2}\) ist.

  Nach \autoref{lemma:4:6} genügt es bei 2 Punkten, nach \autoref{lemma:4:7} genügt es bei 3 Punkten zu zeigen, dass \(p(P_k)\) in einen Kreis mit gesuchter Größe passt, denn mit den Lemmata folgt \(p(P_K) \subseteq p(P_k)\), solange \(P_k\) Teilmenge der konvexen Hülle von \(P_K\) ist. Diese Bedingung ist hier erfüllt, schließlich liegt die Figur von \(P_k\) innerhalb/auf der von \(P_K\).

  Im Fall von 2 Punkten \(A,B\) oder 3 Punkten \(A,B,X\) mit rechtem Winkel bei \(X\) in \(ΔAXB\) ist \(AB\) jeweils ein Durchmesser von \(k\). Damit liegt folgende Situation vor:
  \begin{center}
    \includegraphics[width=0.5\textwidth]{a4-2-6}
  \end{center}

  \(C,D\) sind hier die Schnittpunkte der Kreise um \(A,B\) mit Radius 1. \(ΔBAC\) ist rechtwinklig, denn der Satz des Pythagoras gilt: \(\overline{AB}^2 = 2 = 1 + 1 = \overline{AC}^2+\overline{BC}^2\). \(C\) liegt nach dem Satz des Thales also auf \(k\), gleiches gilt für \(D\). Nach \autoref{lemma:4:5} passt \(p(P_k)\) (die Schnittfläche der Kreisscheiben um \(A\) \& \(B\)) also in den Kreis um \(BDAC\), schließlich liegen die Ecken \(C,D\) auf ihm und die Kreisscheiben von \(A,B\) haben Radius 1, während \(k\) einen Radius von \(\frac{1}{\sqrt{2}}\) hat. Also passt in diesem Fall \(M_b\) in einen Kreis der gesuchten Größe.

  Zeigen wir nun, dass auch im Fall von 3 Punkten, welche ein spitzes Dreieck bilden, \(p(P_k)\) nicht über \(k\) hinausgehen kann. Zeigen wir dazu zunächst, dass keine der Ecken der Schnittfläche außerhalb des Kreises liegen kann. Die Ecken sind Schnittpunkte von mindestens 2 der 3 Kreise um Punkte aus \(P_k\) und liegen zudem im dritten Kreis. Betrachten wir nun 2 der 3 Punkte. Diese teilen den Kreis in 2 Kreisbögen. Da alle Winkel im Dreieck spitz sind, liegen sich in \(k\) keine Punkte aus \(P_k\) gegenüber, weshalb einer der Kreisbögen länger als der andere ist. Nun werden wir zeigen, alle möglichen Ecken von \(p(P_k)\) in \(k\) liegen. Dazu definieren wir zunächst \(D\) als Schnittpunkt der Kreise um die gewählten Punkte, welcher auf der Seite des längeren Kreisbogens liegt.

  \begin{center}
    \includegraphics[width=0.25\textwidth]{a4-2-7}
  \end{center}

  \(D\) liegt offensichtlich auf der Gerade \(QO_K\), wobei \(Q\) hier der Mittelpunkt von \([AB]\) ist. Dabei ist \(\overline{BD} = \overline{DA} = 1\). Wir sehen allerdings auch, dass der Abstand \(\overline{BP}\) größer als 1 ist, denn \(ΔBPO_K\) ist bei \(O_K\) stumpf, d.h. \([BP]\) ist bekanntlich länger als wenn \(ΔBPO_K\) rechtwinklig wäre: \(\overline{BP} > \sqrt{\overline{BO_k}^2+\overline{O_KP}^2} = \sqrt{\frac{1}{2}+\frac{1}{2}} = 1 \) (Satz des Pythagoras). Also liegt \(D\) höher als \(P\) auf \(QO_K\). Da \(D\) gleichzeitig tiefer als \(BA\) liegt, muss sich \(D\) also innerhalb von \(k\) befinden.

  Nun zum Schnittpunkt auf der Seite des kürzeren Kreisbogens, hier genannt \(C\). Wir werden nun zeigen, dass dieser Punkt keine Ecke sein kann:

  \begin{center}
    \includegraphics[width=0.5\textwidth]{a4-2-8}
  \end{center}

  Wieder ist \(Q\) der Mittelpunkt von \([AB]\). Damit \(E\), der dritte Punkt aus \(P_k\), auch maximal 1 entfernt von \(C\) ist, muss er in/auf dem Kreis um \(C\) mit Radius 1 liegen. Also kann \(E\) nur auf \(\overarc{AB}\) auf \(k\) liegen. Liegt jedoch \(E\) dort, so wäre der Winkel beim \(E\) stumpf, da der gegenüberliegende Bogen länger als ein Halbkreis \(k\)s ist. Das Dreieck von \(P_k\) muss allerdings spitz sein, womit ein Widerspruch gefunden wurde. Solch ein Schnittpunkt kann damit keine Ecke sein. Also liegen alle Eckpunkte der Schnittfläche in/auf Kreis \(k\).

  Nach \autoref{lemma:4:5} befindet sich also die ganze Schnittfläche in/auf Kreis \(k\), denn alle Schnittpunkte, welche Ecken der Schnittfläche sind, liegen innerhalb von \(k\) und der Radius der Kreisscheiben ist \(1\), während \(k\) einen Radius von \(\frac{1}{\sqrt{2}}\) hat. Damit liegt also auch \(p(P_K)\) und daher \(M_b\) in/auf \(k\).
\end{proof}

\begin{proof_thm}{\autoref{lemma:4:1}}
  \label{proof_thm:4:1}
  Dieser Beweis wurde von \url{https://en.wikipedia.org/wiki/Smallest-circle_problem#Characterization} (letzter Aufruf am 31. August 2022) übernommen und übersetzt.

  Definieren wir \(P\) als arbiträre Punktemenge in der Ebene und nehmen an, dass es zwei verschiedene kleinste umschließende Kreise um \(P\) mit den Mittelpunkten bei \(\vec{z}_1\) und \(\vec{z}_2\) gibt. Definieren wir nun \(r\) als deren gemeinsamen Radius und \(a\) als den Abstand zwischen ihren Zentren. Da \(P\) eine Teilmenge von beiden Kreisen ist, ist \(P\) eine Teilmenge der Schnittmenge der beiden Kreise. Allerdings ist diese Schnittmenge auch in dem Kreis beinhaltet, welcher sein Zentrum bei \(z_1+z_2\) und den Radius \(r^2-a^2\) hat, wie im folgenden Bild sichtbar ist:
  \begin{center}
    \includegraphics[width=0.5\textwidth]{a4-lem-1}
  \end{center}
  Da \(r\) der kleinst möglichste Radius ist, muss \(r^2-a^2=r^2\) gelten, woraus \(a=0\) folgt. Daher müssen die Kreise identisch sein.
\end{proof_thm}

\begin{proof_thm}{\autoref{lemma:4:5}}
  \label{proof_thm:4:5}
  Schneiden sich keine Kreisscheiben, so ist die Schnittmenge die leere Menge, welche offensichtlich Teilmenge jeder Kreisscheibe ist.

  Die Ecken der Schnittfläche sind offensichtlich Schnittpunkte der Ränder mancher Kreisscheiben, welche in allen Kreisscheiben enthalten sind. Offensichtlich werden diese Ecken durch Kreisbögen verbunden. Dabei können diese Kreisbögen nicht Halbkreise oder etwas längeres sein, da alle Ränder der Schnittfläche auch in anderen Kreisscheiben beinhaltet sein müssen. Halbkreise (oder länger) passen allerdings nur in ihre eigene Scheibe, da alle Scheiben den gleichen Radius haben. Scheiben dürfen nach den Bedingungen des Lemmas allerdings nicht doppelt vorkommen, womit kein Bogen Halbkreis oder etwas längeres sein kann.

  Da alle Kreisbögen einen Radius größer oder gleich dem Kreis haben und nicht länger als ein Halbkreis sind, biegen sie sich nicht aus dem Kreis heraus, selbst wenn ihr Anfangs- und ihr Endpunkt auf dem Kreis selbst liegen. Daher muss der gesamte Pfad und damit auch die abgeschlossene Fläche innerhalb/auf dem Kreis liegen.
\end{proof_thm}

\begin{proof_thm}{\autoref{lemma:4:6}}
  \label{proof_thm:4:6}
  Ist die Schnittfläche der beiden Kreisscheiben leer, so ist der Beweis trivial, schließlich ist die leere Menge Teilmenge einer jeden Menge.
  \begin{center}
    \includegraphics[width=0.5\textwidth]{a4-lem-6-2}
  \end{center}
  Ist dem nicht der Fall, so definieren wir \(A,B\) als die Mittelpunkte der Kreisscheiben, \(C,D\) als die Schnittpunkte der Ränder der Kreisscheiben und \(X\) als einen arbiträren Punkt auf \([AB]\) mit entsprechender Kreisscheibe mit gleichem Radius wie die Scheiben um \(A,B\). Wie an der Abbildung erkennbar ist, liegt jeder Punkt auf der Strecke \([AB]\) nicht weiter als \(1\) von \(C\) entfernt, gleiches gilt für \(D\). Damit müssen \(C, D\), die Ecken der Schnittfläche, also in \(X\)s Kreisscheibe liegen. Zudem haben die Scheiben um \(A,B,X\) alle Radius 1. Nach \autoref{lemma:4:5} liegt also die ganze Schnittfläche auf \(X\)s Kreisscheibe.
\end{proof_thm}

\begin{proof_thm}{\autoref{lemma:4:7}}
  \label{proof_thm:4:7}
  \begin{center}
    \includegraphics[width=0.5\textwidth]{a4-lem-7-2}
  \end{center}
  Nennen wir die Mittelpunkte der drei Kreise \(A,B,C\). Definieren wir nun \(E\) als arbiträren Punkt in/auf dem Dreieck \(ABC\). Nun hat die Linie \(CE\) einen Schnittpunkt mit der Seite \([AB]\). Nennen wir diesen Punkt \(D\). Bezeichnen wir nun mit \(M_x\) die Punktemenge des dem Punkt \(X\) entsprechenden Kreises. Nun wenden wir \autoref{lemma:4:6} auf die Linien \(CD\) und \(AB\) an: \(M_C ∩ (M_A ∩ M_B) \subseteq M_C ∩ M_D \subseteq M_E \).
\end{proof_thm}

\pagebreak
\begin{proof_thm}{\autoref{lemma:4:9}}
  \label{proof_thm:4:9}
  \begin{center}
    \includegraphics[width=0.3\textwidth]{a4-lem-9}
  \end{center}
  Auf dem kleinsten umschließenden Kreis \(K\) von \(P\) kann kein (von Punkten) ununterbrochener Kreisbogen länger als eine Hälfte existieren. Beweisen wir durch Widerspruch: Definieren wir zunächst \(A,B\) als die Punkte an den Enden des überlangen Kreisbogens. Die Strecke \([AB]\) ist offensichtlich kürzer als der Durchmesser des Kreises. Damit existiert allerdings auch ein kleinerer umschließender Kreis \(k\) mit der Strecke \([AB]\) als Durchmesser. Denn die Fläche in welcher die Punkte liegen, wird vom Bogen \(\overarc{AB}\) auf \(K\) und der Strecke \([AB]\) eingeschlossen und ist daher Teil der Schnittfläche von \(K\) mit der Spiegelung von \(K\) an \(AB\) (gestrichelte Linie). Da die Ecken der Schnittfläche \(A,B\) in \(k\) liegen und der Radius von \(K\) größer als der von \(k\) ist, liegt die Fläche und damit alle Punkte nach \autoref{lemma:4:5} in/auf \(k\). Daher wäre \(k\) also wirklich ein kleinerer umschließender Kreis.

  Wählen wir nun einen Punkt \(A\) aus \(P\). Sodann teilen wir den Kreis entlang dem Durchmesser, welcher durch \(A\) geht, in zwei Hälften. Wir wählen auf beiden jeweils einen Punkt, und zwar den Punkt, welcher am weitesten entfernt von \(A\) ist. Dabei muss wegen vorher bewiesener Eigenschaft auf jeder Hälfte ein solcher Punkt ungleich \(A\) existieren und die beiden Punkte können nicht weiter als eine Hälfte eines Kreises von einander entfernt sein. Damit kann bekanntlich kein Winkel stumpf sein, solange die drei Punkte unterschiedlich sind. Sind die beiden weiteren Punkte äquivalent, so sind sie nur ein Punkt und dieser liegt auf dem Kreis gegenüber von \(A\). In diesem Fall wählen wir als dritten Punkt einen arbiträren weiteren Punkt auf dem Kreis, damit ist das Dreieck rechtwinklig.
\end{proof_thm}
\end{document}
